## PROJECT ##

* ID: **H**igh **P**erformance **C**omputing!PRAKTIKUM
* Contact: nikolaj <@> schepsen.eu

## CONTENT ##

#### WEEK 1: Introduction to C++ ####

* The first c++ Program
* The for-Loop
* Memory Allocation
* The new & delete Ops
* Pointers
* Factorial
* Function Arguments
* Arrays
* Templates
* The Epsilon Calculation

#### WEEK 2: Introduction to SIMD ####

* Element-Wise Square Root of Matrices
* The maximum Root of a Quadratic Equation
* SIMDized Sum Calculation

#### WEEK 3: SIMD (Kalman Filter) ####

* Fit of straight, linear Tracks based on the Kalman Filter

#### WEEK 4: Introduction to VC ####

* Memory
* Element-Wise Square Root of Matrices (using Vc)
* The maximum Root of a Quadratic Equation (using Vc)
* Iterative method for finding approximations to the roots of a function
* Gather and Scatter functions

## USAGE ##

To compile these code snippets run cmake . & make

## REQUIREMENTS ##

* CMake 3.0 or higher
* [Vc](https://github.com/VcDevel/Vc) (SIMD vector Classes for C++) 0.7.5 or higher

## CHANGELOG ##

### High Performance Computing!PRAKTIKUM, updated @ 2016-11-09 ###

* Added our solutions for the 3th week tasks

### High Performance Computing!PRAKTIKUM, updated @ 2016-11-02 ###

* Added our solutions for the 2th week tasks


### High Performance Computing!PRAKTIKUM, updated @ 2016-10-26 ###

* Added our solutions for the 1th week tasks
