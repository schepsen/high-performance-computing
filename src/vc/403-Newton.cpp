#include <iostream> // std::cout

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * @name Iterative method for finding approximations to the roots (or zeroes) of a function.
 * @authors: I.Kulakov; M.Zyzak; N.Schepsen
 *
 * TODO: solve the following TASKS:
 *
 * (A) SIMDize using Vector classes
 *
 * (B) Compare results and execution time of all versions
 */

#include <Vc/Vc> // Vector classes

#include <cmath>
#include <stdlib.h>

#include "../../lib/TStopwatch.h"

const int N = 100000;

float par1[N], par2[N] __attribute__ ((aligned(16))), root[N] __attribute__ ((aligned(16)));

const float P = 1e-5; // precision of root computation

template<class T>

T F(const T& x, const T& p1, const T& p2)
{
    const T x2 = x * x;

    return p1 * x2 * x + p2 * x2 + 6 * x + 80;
}

template<class T>

T Fd(const T& x, const T& p1, const T& p2)
{
    return 3 * p1 * x * x + 2 * p2 * x + 6;
}

float FindRootScalar(const float& p1, const float& p2)
{
    float x = 1, x_new = 0;

    for (; std::abs((x_new - x) / x_new) > P; )
    {
        x = x_new; x_new = x - F(x, p1, p2) / Fd(x, p1, p2);
    }

    return x_new;
}

Vc::float_v FindRootVector(const Vc::float_v& p1, const Vc::float_v& p2)
{
    Vc::float_v x = 1.f, x_new = 0.f;

    Vc::float_m mask (true);

    for (; !mask.isEmpty(); )
    {
        x = x_new; x_new(mask) = x - F(x, p1, p2) / Fd(x, p1, p2);

        mask = std::abs((x_new - x) / x_new) > P;
    }

    return x_new;
}

bool CheckResults(float* r)
{
    bool ok = true;

    for (int i = 0; i < N; ++i)
    {
        ok &= std::fabs(F(root[i], par1[i], par2[i]) / Fd(root[i], par1[i], par2[i])) < P;
    }

    return ok;
}

bool CompareResults(float* r1, float* r2)
{
    bool ok = true;

    for (int i = 0; i < N; ++i)
    {
        ok &= std::fabs(r1[i] - r2[i]) < 1e-7;
    }

    return ok;
}

int main(int argc, char** argv)
{
    for (int i = 0; i < N; ++i)
    {
        par1[i] = 1 + double(rand()) / double(RAND_MAX); // from 1 to 2
        par2[i] = double(rand()) / double(RAND_MAX); // from 0 to 1
    }

    /* SCALAR COMPUTATION */

    TStopwatch timer;

    for (int i = 0; i < N; ++i)
    {
        root[i] = FindRootScalar(par1[i], par2[i]);
    }

    timer.Stop();

    std::cout << "== [*HPC-PR*] SCALAR took " << timer.RealTime() * 1000 << " ms\n";
    std::cout << "== [*HPC-PR*] The results are " << ((CheckResults(root) ? "" : "IN")) << "CORRECT" << '\n';

    float timeS = timer.RealTime();

    /* SIMD COMPUTATION */

    const int Nv = N / Vc::float_v::Size;
    float root2[N];
    Vc::float_v par1_v[Nv], par2_v[Nv], root_v[Nv];

    for (int i = 0; i < Nv; ++i)
    {
        par1_v[i].load(par1 + i * Vc::float_v::Size, Vc::Aligned);
        par2_v[i].load(par2 + i * Vc::float_v::Size, Vc::Aligned);
    }

    timer.Start();

    for (int i = 0; i < Nv; ++i)
    {
        root_v[i] = FindRootVector(par1_v[i], par2_v[i]);
    }

    timer.Stop();

    for (int i = 0; i < Nv; ++i)
    {
        root_v[i].store(root2 + i * Vc::float_v::Size, Vc::Aligned);
    }

    float timeV = timer.RealTime();

    std::cout << "== [*HPC-PR*] SIMD took " << timer.RealTime() * 1000 << " ms (" << "Speed Up: " << timeS / timeV << ")" << std::endl;
    std::cout << "== [*HPC-PR*] The SIMD and SCALAR results are" << (CompareResults(root, root2) ? " " : " NOT ") << "the same!" << std::endl;

    return 0;
}
