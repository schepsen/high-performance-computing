#include <iostream>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * @name The maximum root of a quadratic equation a*x^2+b*x+c = 0
 * @authors: I.Kulakov; M.Zyzak; N.Schepsen
 *
 * TODO: solve the following TASKS:
 *
 * (A) Finish the Vc-ized version of this task
 */

#include <Vc/Vc>
#include <cstdio>
#include <cmath>
#include <stdlib.h>

#include "../../lib/TStopwatch.h"

static const int NVectors = 100000, N = NVectors * Vc::float_v::Size, NIterOut = 100;

#define D(A, B, C) B * B - 4 * A * C

struct DataAOSElement
{
    float a, b, c, /* coefficients */ x; /* a root */
};

struct DataAOS
{
    DataAOS(const int N)
    {
        data = (DataAOSElement*)_mm_malloc(sizeof(DataAOSElement) * N, 16);
    }
    ~DataAOS()
    {
        if (data) _mm_free(data);
    }
    DataAOSElement *data;
};

struct DataSOA
{
    DataSOA(const int N)
    {
        a = (float*)_mm_malloc(sizeof(float) * N, 16);
        b = (float*)_mm_malloc(sizeof(float) * N, 16);
        c = (float*)_mm_malloc(sizeof(float) * N, 16);
        x = (float*)_mm_malloc(sizeof(float) * N, 16);
    }
    ~DataSOA()
    {
        if (a) _mm_free(a);
        if (b) _mm_free(b);
        if (c) _mm_free(c);
        if (x) _mm_free(x);
    }

    float *a, *b, *c, /* coefficients */ *x; /* a root */
};

struct DataAOSOAElement
{
    void SetMemory(float *m)
    {
        a = m;
        b = m + Vc::float_v::Size;
        c = m + 2 * Vc::float_v::Size;
        x = m + 3 * Vc::float_v::Size;
    }

    float *a, *b, *c, /* coefficients */ *x; /* a root */
};

struct DataAOSOA
{
    DataAOSOA(const int N)
    {
        const int NVectors = N / Vc::float_v::Size;

        data = new DataAOSOAElement[NVectors];
        memory = (float*)_mm_malloc(sizeof(float) * 4 * N, 16);

        float *memp = memory;
        for (int i = 0; i < NVectors; i++)
        {
            data[i].SetMemory(memp);
            memp += Vc::float_v::Size * 4;
        }
    }
    ~DataAOSOA()
    {
        _mm_free(memory); delete[] data;
    }

    float *memory;
    DataAOSOAElement *data;
};

void CheckResults(const DataAOS& yScalar, const DataAOS& ySIMD, const int NSIMD)
{
    bool ok = 1;

    for (int i = 0; i < N; i++)
    {
        if (fabs(yScalar.data[i].x - ySIMD.data[i].x) > yScalar.data[i].x * 0.01)
        {
            ok = 0;

            printf("%d\t%.3f\t%.3f\t%.3f\n", i, yScalar.data[i].x, ySIMD.data[i].x, fabs(yScalar.data[i].x - ySIMD.data[i].x));
        }
    }

    std::cout << "== [*HPC-PR*] The SIMD " << NSIMD << " and SCALAR results are" << (ok ? " " : " NOT ") << "the same!" << std::endl;
}

void CheckResults(const DataAOS& yScalar, const DataSOA& ySIMD, const int NSIMD)
{
    bool ok = 1;

    for (int i = 0; i < N; i++)
    {
        if (fabs(yScalar.data[i].x - ySIMD.x[i]) > yScalar.data[i].x * 0.01)
        {
            ok = 0;

            printf("%d\t%.3f\t%.3f\t%.3f\n", i, yScalar.data[i].x, ySIMD.x[i], fabs(yScalar.data[i].x - ySIMD.x[i]));
        }
    }

    std::cout << "== [*HPC-PR*] The SIMD " << NSIMD << " and SCALAR results are" << (ok ? " " : " NOT ") << "the same!" << std::endl;
}

void CheckResults(const DataAOS& yScalar, const DataAOSOA& ySIMD, const int NSIMD)
{
    bool ok = 1;

    for (int i = 0; i < N; i++)
    {
        const int nV = i / Vc::float_v::Size;
        const int iV = i % Vc::float_v::Size;
        if (fabs(yScalar.data[i].x - ySIMD.data[nV].x[iV]) > yScalar.data[i].x * 0.01)
        {
            ok = 0;

            printf("%d\t%.3f\t%.3f\t%.3f\n", i, yScalar.data[i].x, ySIMD.data[nV].x[iV], fabs(yScalar.data[i].x - ySIMD.data[nV].x[iV]));
        }
    }

    std::cout << "== [*HPC-PR*] The SIMD " << NSIMD << " and SCALAR results are" << (ok ? " " : " NOT ") << "the same!" << std::endl;
}

int main(int argc, char** argv)
{
    DataAOS dataScalar(N);
    DataAOS dataSIMD1(N);
    DataSOA dataSIMD2(N);
    DataAOSOA dataSIMD3(N);

    for (int i = 0; i < N; i++)
    {
        float a = 0.01 + float(rand()) / float(RAND_MAX); // put a random value, from 0.01 to 1.01 (a has not to be equal 0)
        float b = float(rand()) / float(RAND_MAX);
        float c = -float(rand()) / float(RAND_MAX);

        dataScalar.data[i].a = a;
        dataScalar.data[i].b = b;
        dataScalar.data[i].c = c;

        dataSIMD1.data[i].a = a;
        dataSIMD1.data[i].b = b;
        dataSIMD1.data[i].c = c;

        dataSIMD2.a[i] = a;
        dataSIMD2.b[i] = b;
        dataSIMD2.c[i] = c;

        const int nV = i / Vc::float_v::Size;
        const int iV = i % Vc::float_v::Size;
        dataSIMD3.data[nV].a[iV] = a;
        dataSIMD3.data[nV].b[iV] = b;
        dataSIMD3.data[nV].c[iV] = c;
    }

    TStopwatch timerScalar;

    for (int io = 0; io < NIterOut; io++)
    {
        for (int i = 0; i < N; i++)
        {
            float &a = dataScalar.data[i].a;
            float &b = dataScalar.data[i].b;
            float &c = dataScalar.data[i].c;
            float &x = dataScalar.data[i].x;

            float det = b * b - 4 * a * c;
            x = (-b + sqrt(det)) / (2 * a);
        }
    }
    timerScalar.Stop();

/* SIMD calculations with Vc using dataSIMD1 as input */

    TStopwatch timerSIMD1;

    for (int io = 0; io < NIterOut; io++)
    {
        for (int i = 0; i < NVectors; i++)
        {
            Vc::float_v a, b, c;

            for (int off = 0; off < Vc::float_v::Size; ++off)
            {
                a[off] = dataSIMD1.data[i * Vc::float_v::Size + off].a;
                b[off] = dataSIMD1.data[i * Vc::float_v::Size + off].b;
                c[off] = dataSIMD1.data[i * Vc::float_v::Size + off].c;
            }

            Vc::float_v result = (-b + sqrt(D(a, b, c))) / (2 * a);

            for(int off = 0; off < Vc::float_v::Size; ++off)
            {
                dataSIMD1.data[i * Vc::float_v::Size + off].x = result[off];
            }
        }
    }

    timerSIMD1.Stop();

/* SIMD calculations with Vc using dataSIMD2 as input (use cast here) */

    TStopwatch timerSIMD2;

    for (int io = 0; io < NIterOut; io++)
    {
        for (int i = 0; i < N; i += Vc::float_v::Size)
        {
            Vc::float_v& a = (reinterpret_cast<Vc::float_v&>(dataSIMD2.a[i]));
            Vc::float_v& b = (reinterpret_cast<Vc::float_v&>(dataSIMD2.b[i]));
            Vc::float_v& c = (reinterpret_cast<Vc::float_v&>(dataSIMD2.c[i]));

            Vc::float_v& result = (reinterpret_cast<Vc::float_v&>(dataSIMD2.x[i]));

            result = (-b + sqrt(D(a, b, c))) / (2 * a);
        }
    }

    timerSIMD2.Stop();

/* SIMD calculations with Vc using dataSIMD2 as input (use cast here) */

    TStopwatch timerSIMD3;

    for (int io = 0; io < NIterOut; io++)
    {
        for (int i = 0; i < NVectors; i++)
        {
            Vc::float_v& a = *(reinterpret_cast<Vc::float_v*>(dataSIMD3.data[i].a));
            Vc::float_v& b = *(reinterpret_cast<Vc::float_v*>(dataSIMD3.data[i].b));
            Vc::float_v& c = *(reinterpret_cast<Vc::float_v*>(dataSIMD3.data[i].c));

            Vc::float_v& result = *(reinterpret_cast<Vc::float_v*>(dataSIMD3.data[i].x));

            result = (-b + sqrt(D(a, b, c))) / (2 * a);
        }
    }

    timerSIMD3.Stop();

    double tScal = timerScalar.RealTime() * 1000;

    double tAOS = timerSIMD1.RealTime() * 1000, tSOA = timerSIMD2.RealTime() * 1000, tAOSOA = timerSIMD3.RealTime() * 1000;

    std::cout << "== [*HPC-PR*] SCALAR took: " << tScal << " ms " << std::endl;
    CheckResults(dataScalar, dataSIMD1, 1);
    std::cout << "== [*HPC-PR*] Vc AOS took: " << tAOS << " ms, speed up " << tScal / tAOS << std::endl;
    CheckResults(dataScalar, dataSIMD2, 2);
    std::cout << "== [*HPC-PR*] Vc SOA took: " << tSOA << " ms, speed up " << tScal / tSOA << std::endl;
    CheckResults(dataScalar, dataSIMD3, 3);
    std::cout << "== [*HPC-PR*] Vc AOSOA took: " << tAOSOA << " ms, speed up " << tScal / tAOSOA << std::endl;

    return 0;
}
