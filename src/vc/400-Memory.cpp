#include <iostream>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * @name Using Memory, based on Vc Manual: http://code.compeng.uni-frankfurt.de/docs/Vc-0.4.0/
 * @authors: I.Kulakov; M.Zyzak
 *
 */

 #include <Vc/Vc>

int main(int argc, char** argv)
{
    const int NV = 3, N = 11;

    Vc::int_v x[NV];

    int in[N] /* copy from */, out[N]; // copy in;

    for (int i = 0; i < N; ++i) in[i] = i;

    {
        Vc::Memory<Vc::int_v, N> array; // aligned memory
        // scalar access:
        for (int i = 0; i < array.entriesCount(); ++i)
        {
            array[i] = in[i]; // write
        }
        // vector access:
        for (int i = 0; i < array.vectorsCount(); ++i)
        {
            x[i] = array.vector(i); // read
        }
        for (int i = 0; i < array.vectorsCount(); ++i)
        {
            x[i] *= 2;
        }
        for (int i = 0; i < array.vectorsCount(); ++i)
        {
            array.vector(i) = x[i]; // write
        }
        // scalar access:
        for (int i = 0; i < array.entriesCount(); ++i)
        {
            out[i] = array[i]; // read
        }
    }

    std::cout << "== [*HPC-PR*] Memory" << std::endl;

    for (int i = 0; i < N; ++i) std::cout << out[i] << ((i == N - 1) ? "\n" : " ");

    {
        int array[12] __attribute__ ((aligned(16)));

        for (int i = 0; i < NV; ++i)
        {
            x[i].load(in + i * Vc::int_v::Size, Vc::Aligned);
        }
        // do something
        for (int i = 0; i < NV; ++i)
        {
            x[i] *= 2;
        }
        for (int i = 0; i < NV; ++i)
        {
            x[i].store(out + i * Vc::int_v::Size, Vc::Aligned);
        }
    }

    std::cout << "== [*HPC-PR*] Load & Store" << std::endl;

    for (int i = 0; i < N; ++i) std::cout << out[i] << ((i == N - 1) ? "\n" : " "); return 0;
}
