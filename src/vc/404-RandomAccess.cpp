#include <iostream>
#include <cstdio>
#include <stdlib.h>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * @name Gather and Scatter functions
 * @authors: I.Kulakov; M.Zyzak; N.Schepsen
 *
 * TODO: solve the following TASKS:
 *
 * (A) Finish the Vc-version of this programm
 *
 */

#include <Vc/Vc>

const int N = 100; // N should be divisible by 4

float input[N], output[N];

int main(int argc, char** argv)
{
    unsigned int index[Vc::float_v::Size]; srand(time(NULL));

    /* fill input array with random numbers from 0 to 1 */

    for( int i = 0; i < N; i++ )
    {
        input[i] = float(rand())/float(RAND_MAX);
    }

    /* fill output array with 0 */

    for( int i = 0; i < N; i++ )
    {
        output[i] = 0;
    }

    /* fill indices with random numbers from 0 to N-1 */

    for( int i = 0; i < Vc::float_v::Size; i++)
    {
        index[i] = static_cast<unsigned int>(float(rand())/float(RAND_MAX)*N);
    }

    std::cout << "== [*HPC-PR*] Indices: ";

    for(int i = 0; i < Vc::float_v::Size; i++) std::cout << index[i] << ((i == Vc::float_v::Size - 1) ? "\n" : " ");

    /* Gathers without masking */

    Vc::float_v tmp; tmp.gather(input, index);

    bool ok = 1;

    for(int i = 0; i < Vc::float_v::Size; i++)
    {
        if(std::fabs(tmp[i] - input[index[i]]) > 1.e-7) ok=0;
    }

    std::cout << "== [*HPC-PR*] Gather without Masking: " << (ok ? "OK" : "WRONG") << "\n";

    /* Gathers with masking */

    Vc::float_v tmp2; Vc::float_m mask(tmp > 0.5f);

    tmp2.gather(input, index, mask);

    ok = 1;

    for(int i = 0; i < Vc::float_v::Size; i++)
    {
        if(input[index[i]] > 0.5f)
        {
            if(std::fabs(tmp2[i] - input[index[i]]) > 1.e-7)
            {
                ok = 0;

                std::cout << "== [*HPC-PR*] " << tmp2[i] << " is not equal to " << input[index[i]] << std::endl;
            }
        }
        else
        {
            if(fabs(tmp2[i]) > 1.e-7)
            {
                ok = 0;

                std::cout << "== [*HPC-PR*] " << tmp2[i] << " is not equal to 0" << std::endl;
            }
        }
    }

    std::cout << "== [*HPC-PR*] Gather with Masking: " << (ok ? "OK" : "WRONG") << "\n";

    mask = (tmp < 0.5f); tmp.scatter(output, index, mask);

    ok = 1;

    for(int i = 0; i < Vc::float_v::Size; i++)
    {
        if(tmp[i] < 0.5f)
        {
            if(std::fabs(tmp[i] - output[index[i]]) > 1.e-7)
            {
                ok = 0;

                std::cout << "== [*HPC-PR*] " << tmp[i] << " is not equal to " << input[index[i]] << std::endl;
            }
        }
        else
        {
            if(std::fabs(output[index[i]]) > 1.e-7)
            {
                ok = 0;

                std::cout << "== [*HPC-PR*] " << output[index[i]] << " is not equal to 0" << std::endl;
            }
        }
    }

    std::cout << "== [*HPC-PR*] Scatter with Masking: " << (ok ? "OK" : "WRONG") << "\n"; return 0;
}
