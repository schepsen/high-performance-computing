#include <iostream>
#include <cmath>
#include <iomanip>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * TODO: solve the following TASKS:
 *
 * (A) The machine epsilon calculates the smallest value of E
 *
 * (B) Estimate the machine epsilon for float, double, long double types
 */

template<class T> T Epsilon()
{
	const T one = 1;

	T e = one;

	for (T oneP = one + e / 2; std::abs(oneP - one) > 0; oneP = one + e / 2)
	{
		e /= 2;
	}

	return e;
}

int main(int argc, char** argv)
{
	std::cout << "The machine epsilon (float) calculated " <<
			Epsilon<float>() << std::endl;

	std::cout << "The machine epsilon (double) is " <<
			Epsilon<double>() << std::endl;

	std::cout << "The machine epsilon (long double) is " <<
			Epsilon<long double>() << std::endl;

	std::cout << "The machine epsilon (int) is" << Epsilon<int>() << std::endl; return 0;
}
