#include<iostream>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * TODO: solve the following TASKS:
 *
 * (A) Will this program work?
 *
 * (B) Find and fix 2 bugs in this code
 */

void piPointer1(float* pi)
{
	*pi = 3.14;
}

float* piPointer2()
{
	float* pi = new float; *pi = 3.1415;

	return pi;
}

int main(int argc, char** argv)
{
	float* pi1 = new float;

	piPointer1(pi1);

	std::cout << "pi1: "<< *pi1 << std::endl;

	delete pi1;

	float* pi2 = piPointer2();

	std::cout << "pi2: " << *pi2 << std::endl;

    delete pi2; return 0;
}
