#include<iostream>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * TODO: solve the following TASKS:
 *
 * (A) Will this program work?
 *
 * (B) Find 2 bugs and fix them with minimum changes as possible
 */

const int N = 10;

// Get factorial set of the first N numbers

int* GetFactorials()
{
	int *a = new int[N];

	a[0] = 1;

	for (int i = 1; i < N; ++i) a[i] = i * a[i - 1]; return a;
}

int main(int argc, char** argv)
{
	int *a = GetFactorials();

	for (int i = 0; i < N; ++i)
    {
		std::cout << "fact(" << i <<") = " << a[i] << std::endl;
    }

	delete[] a; return 0;
}
