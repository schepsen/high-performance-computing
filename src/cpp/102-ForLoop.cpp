#include <iostream>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * TODO: solve the following TASKS:
 *
 * (A) Run this code and explain it
 */

int main(int argc, char** argv)
{
	for (int n = 10; n > 0; n--)
	{
		std::cout << n << ", ";

		if (n == 1) std::cout << "FIRE!" << std::endl;

	}

	return 0;
}
