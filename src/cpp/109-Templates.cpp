#include <iostream>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * TODO: solve the following TASKS:
 *
 * (A) Will this program work?
 *
 * (B) Run this code and explain the output
 */

template<typename T> T GetMax(T a, T b)
{
	return (a > b) ? a : b;
}

int main(int argc, char** argv)
{
	int i = 5, j = 6; double l = 9.2, m = 2e9;

	std::cout << "max(" << i << "," << j << ") = " << GetMax<int>   (i, j) << std::endl;
	std::cout << "max(" << l << "," << m << ") = " << GetMax<double>(l, m) << std::endl;

	std::cout << "as float: " << GetMax<float>(l, m) << std::endl;
	std::cout << "as integer: " << GetMax<int>(l, m) << std::endl;
	std::cout << "as short: " << GetMax<short>(l, m) << std::endl;

	std::cout << "as char: " << int(GetMax<char>(l, m)) << std::endl;

	return 0;
}
