#include<iostream>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * TODO: solve the following TASKS:
 *
 * (A) Will this program work?
 *
 * (B) Finish the program and run it
 */

void increase1(int arg)
{
	arg++;
}

int increase2(int arg)
{
	arg++; return arg;
}

void pointer_increase(int* arg)
{
	(*arg)++;
}

void reference_increase(int& arg)
{
	arg++;
}

int main(int argc, char** argv)
{
	int number = 0;

	std::cout << "The value is: " << number << std::endl;
	increase1(number); // Has no effect.
	std::cout << "The value is: " << number << std::endl;
	number = increase2(number);
	std::cout << "The value is: " << number << std::endl;
	pointer_increase(&number);
	std::cout << "The value is: " << number << std::endl;
	reference_increase(number);
	std::cout << "The value is: " << number << std::endl;

	return 0;
}
