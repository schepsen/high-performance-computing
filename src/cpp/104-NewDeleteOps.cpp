#include <iostream>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * TODO: solve the following TASKS:
 *
 * (A) Will this program work?
 *
 * (B) Run and explain the output
 *
 * (C) Explain the behavior of the program when you change k from 1 to 3
 */

const short k = 1;

int main(int argc, char** argv)
{
	float* p = new float(123);

	float* p1 = p; p1++;

	// PART 1

	std::cout << "# PART 1" << std::endl;

	std::cout << "t1 -> p @ " << p << "\t" << *p << std::endl;
	(*p)++;
	std::cout << "t2 -> p @ " << p << "\t" << *p << std::endl;

	// PART 2

	std::cout << "# PART 2" << std::endl;

	std::cout << "t1 -> p @ " << p << "\t" << *p << std::endl;
	delete p;
	std::cout << "t2 -> p @ " << p << "\t" << *p << std::endl;

	return 0;
}
