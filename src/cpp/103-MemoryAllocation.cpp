#include<iostream>
#include<cmath>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * TODO: solve the following TASKS:
 *
 * (A) Finish the program, run it and explain the output
 */

#include "xmmintrin.h"

int main(int argc, char** argv)
{
	// When the size of the array is known

	const int SIZE = 10; float Array_static[SIZE];

	for (int i = 0; i < SIZE; ++i)
	{
		Array_static[i] = sin(i) * 10.f + i;
	}

	// When the size is unknown

	int size;

	std::cout << "Define the size of arrays (dynamic, aligned): ";
	std::cin >> size;
	std::cout << std::endl;

	float* Array_dynamic = new float[size];

	for (int i = 0; i < size; ++i)
	{
		Array_dynamic[i] = sin(i) * 10.f + i;
	}

	float* Array_dynamic_aligned = (float*) _mm_malloc(sizeof(float) * size, 16 * 16 * 16);

	for (int i = 0; i < size; ++i)
	{
		Array_dynamic_aligned[i] = sin(i) * 10.f + i;
	}

	// Print the Array_static array to the screen

	std::cout << "Array (static) \t\t\t";

	for (int i = 0; i < SIZE; ++i)
	{
		std::cout << Array_static[i] << ((i == SIZE - 1) ? "\n" : " ");
	}

	// Print the Array_dynamic array to the screen

	std::cout << "Array (dynamic) \t\t";

	for (int i = 0; i < size; ++i)
	{
		std::cout << Array_dynamic[i] << ((i == size - 1) ? "\n" : " ");
	}

	// Print the Array_dynamic_aligned array to the screen

	std::cout << "Array (dynamic, aligned) \t";

	for (int i = 0; i < size; ++i)
	{
		std::cout << Array_dynamic_aligned[i] << ((i == size - 1) ? "\n" : " ");
	}

	// The First Element

	std::cout << std::endl << "*Array_dynamic (then):   " << *Array_dynamic << std::endl;
	*Array_dynamic = 42.3f;
	std::cout << "*Array_dynamic (now):   " << *Array_dynamic << std::endl;

	// And the *Array_dynamic really points to the first element:

	std::cout << "Array_dynamic[0] (now): " << Array_dynamic[0] << std::endl;

	std::cout << "\nArray positions in memory: " << Array_static << "\t" << Array_dynamic << "\t" << Array_dynamic_aligned << std::endl;

	// Free the memory for Array_dynamic and Array_dynamic_aligned

	delete[] Array_dynamic; _mm_free(Array_dynamic_aligned);

	return 0;
}
