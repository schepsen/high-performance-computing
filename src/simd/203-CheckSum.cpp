#include <iostream>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * @name Check parallel sum calculation
 * @authors: I.Kulakov; M.Zyzak; N.Schepsen
 *
 * TODO: solve the following TASKS:
 *
 * (A) Finish the sum calculation using SIMD
 *
 * (B) Finish the sum calculation using a usual way
 */

#include <stdlib.h>

// SSE Instruction

#include "../../lib/vectors/P4_F32vec4.h"
#include "../../lib/TStopwatch.h"

// N should be divisible by 4

const int NIter = 100, N = 4000000;

unsigned char str[N] __attribute__ ((aligned(16)));

#define FVECLENCHAR fvecLen * 4

template<typename T> T sum(const T* data, const int N)
{
    T tmp = 0;

    for (int i = 0; i < N; ++i)
    {
        tmp = tmp ^ data[i];
    }

    return tmp;
}

int main(int argc, char** argv)
{
    srand(time(NULL)); /* initializes random number generator */

    for (int i = 0; i < N; ++i)
    {
        str[i] = 256 * (double(rand()) / RAND_MAX);
    }

    unsigned char sumA = 0;

    TStopwatch timerScalar;

    for (int i = 0; i < NIter; ++i)
    {
        sumA = sum<unsigned char>(str, N);
    }

    timerScalar.Stop();

    unsigned char sumV = 0;

    TStopwatch timerSIMD;

    for (int i = 0; i < NIter; ++i)
    {
        fvec fV
        (
            sum<fvec>(reinterpret_cast<fvec*>(str), N >> 4)
        );

        unsigned char *charV(reinterpret_cast<unsigned char*>(&fV));

        sumV = charV[0];

        for (int index = 1; index < 16; ++index) sumV ^= charV[index];
    }

    timerSIMD.Stop();

    unsigned char sumI = 0;

    TStopwatch timerSCALARINT;

    for (int i = 0, tmp; i < NIter; ++i)
    {
        tmp = sum<int>(reinterpret_cast<int*>(str), N >> 2);

        unsigned char *charS
        (
            reinterpret_cast<unsigned char*>(&tmp)
        );

        sumI = charS[0];

        for (int index = 1; index < 4; ++index) sumI ^= charS[index];
    }

    timerSCALARINT.Stop();

    double caseZ = timerScalar.RealTime() * 1000;
    double caseA = timerSIMD.RealTime() * 1000;
    double caseB = timerSCALARINT.RealTime() * 1000;

    std::cout << "== [*HPC-PR*] SCALAR took " << caseZ << " ms (used as reference)" << std::endl;

    std::cout << "== [*HPC-PR*] The sums SCALAR and SIMD are" << ((sumA == sumV) ? " " : " NOT ") << "equal!" << std::endl;
    std::cout << "== [*HPC-PR*] SIMD took " << caseA << " ms (" << "Speed Up: " << caseZ / caseA << ")" << std::endl;
    std::cout << "== [*HPC-PR*] The sums SCALAR and SCALAR INTEGER are" << ((sumA == sumI) ? " " : " NOT ") << "equal!" << std::endl;
    std::cout << "== [*HPC-PR*] SCALAR INTEGER took " << caseB << " ms (" << "Speed Up: " << caseZ / caseB << ")" << std::endl;

    return 0;
}
