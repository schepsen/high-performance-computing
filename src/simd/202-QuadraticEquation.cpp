#include <iostream>
#include <cmath>
#include <cstdio>
#include <stdlib.h>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * @name The maximum root of a quadratic equation a*x^2+b*x+c = 0
 * @authors: I.Kulakov; M.Zyzak; N.Schepsen
 *
 * TODO: solve the following TASKS:
 *
 * (A) Finish the task using:
 *
 *   => SIMD intrinsics and copying the data to SIMD vectors
 *   => SIMD intrinsics and casting the data from the scalar arrays to SIMD vectors
 *   => Header files and copying the data to SIMD vectors
 *   => Header files and casting back the data from the scalar arrays to SIMD vectors
 *
 * (B) Set NVectors to 10000000 and NIterOut to 10 and re-run
 *
 * Note?!:
 *
 * SIMD Intrinsics:
 *
 * => _mm_set_ps(f3,f2,f1,f0) - write 4 floats into the SIMD vector
 * => _mm_set_ps1(a) - write float "a" into the SIMD vector
 * => _mm_add_ps(a,b) - a + b
 * => _mm_sub_ps(a,b) - a - b
 * => _mm_mul_ps(a,b) - a * b
 * => _mm_div_ps(a,b) - a / b
 * => _mm_sqrt_ps(a) - sqrt(a)
 *
 * __m128 - SIMD vector
 *
 * type2* pointer2 = reinterpret_cast<type2*>( pointer1 ) - change pointer type
 *
 */

#include "xmmintrin.h"

// SSE Instruction

#include "../../lib/vectors/P4_F32vec4.h"
#include "../../lib/TStopwatch.h"

// The discriminant of the quadratic polynomial

#define D(A, B, C) B * B - 4 * A * C

static const int NVectors = 10000, N = NVectors * fvecLen, NIterOut = 1000;

void compare(const float* yScalar, const float* ySIMD, const int NSIMD)
{
    bool ok = 1;

    for (int i = 0; i < N; ++i)
    {
        if (fabs(yScalar[i] - ySIMD[i]) > yScalar[i] * 0.01)
        {
            ok = 0;

            printf("%d\t%.3f\t%.3f\t%.3f\n", i, yScalar[i], ySIMD[i], fabs(yScalar[i] - ySIMD[i]));
        }
    }

    std::cout << "== [*HPC-PR*] The SIMD " << NSIMD << " and SCALAR results are" << (ok ? " " : " NOT ") << "the same!" << std::endl;
}

int main(int argc, char** argv)
{
    srand(time(NULL)); /* Initializes random number generator */

    float* a = (float*) _mm_malloc(sizeof(float) * N, 16);
    float* b = (float*) _mm_malloc(sizeof(float) * N, 16);
    float* c = (float*) _mm_malloc(sizeof(float) * N, 16);

    float* x = (float*) _mm_malloc(sizeof(float) * N, 16);

    float* x_simd1 = (float*) _mm_malloc(sizeof(float) * N, 16);
    float* x_simd2 = (float*) _mm_malloc(sizeof(float) * N, 16);
    float* x_simd3 = (float*) _mm_malloc(sizeof(float) * N, 16);
    float* x_simd4 = (float*) _mm_malloc(sizeof(float) * N, 16);

    for (int i = 0; i < N; ++i)
    {
        a[i] = +float(rand()) / float(RAND_MAX);
        b[i] = +float(rand()) / float(RAND_MAX);
        c[i] = -float(rand()) / float(RAND_MAX);
    }

    TStopwatch timeScalar;

    for (int t = 0; t < NIterOut; ++t)
    {
        for (int i = 0; i < N; ++i)
        {
            x[i] = (-b[i] + std::sqrt((float) D(a[i], b[i], c[i]))) / (2 * a[i]);
        }
    }

    timeScalar.Stop();

    /* SIMD calculations with SIMD intrinsics and data copy */

    TStopwatch timerSIMD1;

    for (int t = 0; t < NIterOut; ++t)
    {
        for (int i = 0; i < NVectors; ++i)
        {
            __m128 vA = _mm_set_ps
                (
                    a[i * fvecLen + 3],
                    a[i * fvecLen + 2],
                    a[i * fvecLen + 1],
                    a[i * fvecLen + 0]
                );
            __m128 vB = _mm_set_ps
                (
                    b[i * fvecLen + 3],
                    b[i * fvecLen + 2],
                    b[i * fvecLen + 1],
                    b[i * fvecLen + 0]
                );
            __m128 vC = _mm_set_ps
                (
                    c[i * fvecLen + 3],
                    c[i * fvecLen + 2],
                    c[i * fvecLen + 1],
                    c[i * fvecLen + 0]
                );

            const __m128 det = _mm_sub_ps
            (
                _mm_mul_ps(vB, vB), _mm_mul_ps(_mm_set_ps1(4),_mm_mul_ps(vA, vC))
            );

            __m128 vR = _mm_div_ps(_mm_sub_ps(_mm_sqrt_ps(det), vB),_mm_mul_ps(_mm_set_ps1(2), vA));

            for(int index = 0; index < fvecLen; ++index)
            {
                x_simd1[i * fvecLen + index] = (reinterpret_cast<float*>(&vR))[index];
            }
        }
    }

    timerSIMD1.Stop();

    /* SIMD calculations with SIMD intrinsics and reinterpret_cast */

    TStopwatch timerSIMD2;

    for (int t = 0; t < NIterOut; ++t)
    {
        for (int i = 0; i < N; i += fvecLen)
        {
            __m128& vA = (reinterpret_cast<__m128&>(a[i]));
            __m128& vB = (reinterpret_cast<__m128&>(b[i]));
            __m128& vC = (reinterpret_cast<__m128&>(c[i]));

            __m128& vR = (reinterpret_cast<__m128&>(x_simd2[i]));

            const __m128 det = _mm_sub_ps
            (
                _mm_mul_ps(vB, vB), _mm_mul_ps(_mm_set_ps1(4),_mm_mul_ps(vA, vC))
            );

            vR = _mm_div_ps(_mm_sub_ps(_mm_sqrt_ps(det), vB),_mm_mul_ps(_mm_set_ps1(2), vA));
        }
    }

    timerSIMD2.Stop();

    /* SIMD calculations with headers and data copy */

    TStopwatch timerSIMD3;

    for (int t = 0; t < NIterOut; ++t)
    {
        for (int i = 0; i < NVectors; ++i)
        {
            fvec vA
            (
                a[i * fvecLen + 0],
                a[i * fvecLen + 1],
                a[i * fvecLen + 2],
                a[i * fvecLen + 3]
            );
            fvec vB
            (
                b[i * fvecLen + 0],
                b[i * fvecLen + 1],
                b[i * fvecLen + 2],
                b[i * fvecLen + 3]
            );
            fvec vC
            (
                c[i * fvecLen + 0],
                c[i * fvecLen + 1],
                c[i * fvecLen + 2],
                c[i * fvecLen + 3]
            );

            fvec vR = (-vB + sqrt(D(vA, vB, vC))) / (2 * vA);

            for(int index = 0; index < fvecLen; ++index)
            {
                x_simd3[i * fvecLen + index] = vR[index];
            }
        }
    }

    timerSIMD3.Stop();

    /* SIMD calculations with headers and reinterpret_cast */

    TStopwatch timerSIMD4;

    for (int t = 0; t < NIterOut; ++t)
    {
        for (int i = 0; i < N; i += fvecLen)
        {
            fvec& vA
            (
                reinterpret_cast<fvec&>(a[i])
            );
            fvec& vB
            (
                reinterpret_cast<fvec&>(b[i])
            );
            fvec& vC
            (
                reinterpret_cast<fvec&>(c[i])
            );

            fvec& vR(reinterpret_cast<fvec&>(x_simd4[i]));

            vR = (-vB + sqrt(D(vA, vB, vC))) / (2 * vA);
        }
    }

    timerSIMD4.Stop();

    double caseZ = timeScalar.RealTime() * 1000;
    double caseA = timerSIMD1.RealTime() * 1000;
    double caseB = timerSIMD2.RealTime() * 1000;
    double caseC = timerSIMD3.RealTime() * 1000;
    double caseD = timerSIMD4.RealTime() * 1000;

    std::cout << "== [*HPC-PR*] SCALAR took " << caseZ << " ms (used as reference)" << std::endl;

    compare(x, x_simd1, 1);
    std::cout << "== [*HPC-PR*] SIMD 1 took " << caseA << " ms (" << "Speed Up: " << caseZ / caseA << ")" << std::endl;
    compare(x, x_simd2, 2);
    std::cout << "== [*HPC-PR*] SIMD 2 took " << caseB << " ms (" << "Speed Up: " << caseZ / caseB << ")" << std::endl;
    compare(x, x_simd3, 3);
    std::cout << "== [*HPC-PR*] SIMD 3 took " << caseC << " ms (" << "Speed Up: " << caseZ / caseC << ")" << std::endl;
    compare(x, x_simd4, 4);
    std::cout << "== [*HPC-PR*] SIMD 4 took " << caseD << " ms (" << "Speed Up: " << caseZ / caseD << ")" << std::endl;

    return 0;
}
