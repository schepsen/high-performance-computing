#include <iostream>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * @name Element-Wise Square Root of a Matrix
 * @authors: I.Kulakov; M.Zyzak; N.Schepsen
 *
 * TODO: solve the following TASKS:
 *
 * (A) Finish the SIMDized version
 *
 * (B) Compare results and execution time of the both version
 */

#include <stdlib.h>

// SSE Instruction

#include "../../lib/vectors/P4_F32vec4.h"
#include "../../lib/TStopwatch.h"

// N should be divisible by 4

const int N = 1000, NIter = 100;

/*
 * a, initial array filled with random numbers
 * c, scalar computation results
 *
 * c_simd, array for the simd computation results
 */

float a[N][N] __attribute__((aligned(16))), c[N][N], c_simd[N][N] __attribute__((aligned(16)));

template<typename T> T f(T x)
{
    return sqrt(x);
}

void compare(const float a1[N][N], const float a2[N][N])
{
    bool ok = 1;

    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            if (fabs(a1[i][j] - a2[i][j]) > 1.e-8) ok = 0;
        }
    }

    std::cout << "== [*HPC-PR*] The results are " << (ok ? "" : "NOT") << "the same!" << std::endl;
}

int main(int argc, char** argv)
{
    srand(time(NULL)); /* initializes random number generator */

    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            a[i][j] = float(rand()) / float(RAND_MAX); /* (0, 1] */
        }
    }

    /* Starte ab hier die skalare Version der Berechnung  */

    TStopwatch timerScalar;

    for (int loop = 0; loop < NIter; ++loop)
    {
        for (int i = 0; i < N; ++i)
        {
            for (int j = 0; j < N; ++j) { c[i][j] = f(a[i][j]); }
        }
    }

    timerScalar.Stop();

    /* Starte ab hier die SIMD-Version der Berechnung */

    TStopwatch timerSIMD;

    for (int loop = 0; loop < NIter; ++loop)
    {
        for (int i = 0; i < N; ++i)
        {
            for (int j = 0; j < N; j += fvecLen)
            {
                fvec &cVec = reinterpret_cast<fvec&>
                (
                    c_simd[i][j]
                );

                cVec = f(reinterpret_cast<fvec&>(a[i][j]));
            }
        }
    }

    timerSIMD.Stop();

    double caseA = timerScalar.RealTime() * 1000, caseB = timerSIMD.RealTime() * 1000;

    compare(c, c_simd);

    std::cout << "== [*HPC-PR*] SCALAR took " << caseA << " ms" << std::endl;
    std::cout << "== [*HPC-PR*] SIMD took " << caseB << " ms (" << "Speed Up: " << caseA / caseB << ")" << std::endl;

    return 0;
}
