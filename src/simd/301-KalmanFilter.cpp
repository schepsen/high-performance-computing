#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <stdlib.h>

/*
 * High Performance Computing!PRAKTIKUM WS 2016
 *
 * @name Fit of linear tracks based on the Kalman Filter
 * @authors: I.Kulakov; M.Zyzak; N.Schepsen
 *
 * TODO: solve the following TASKS:
 *
 * (A) SIMDize track fitting procedure using templates
 *
 * (B) Compare results and their execution time
 */

#include "../../lib/TStopwatch.h"

// SSE Instruction

#include "../../lib/vectors/P4_F32vec4.h"

enum LFDistributionType { kUniform };

#define OUTPUT
#define TIME
#define SIMDIZED

const int NAN0 = -100000;
const float InfX = 1000, InfTx = 1000;

// Parameters

const int DNStations = 6; // number of stations
const float DDZ = 10; // distance between stations in standard geometry
const float DSigma = 0.5; // sigma of hit distribution

template <typename T> struct LFPoint
{
    LFPoint() : x(NAN0), z(NAN0)
    {
    }

    LFPoint(T x_, T z_) : x(x_), z(z_)
    {
    }

    T x; // x-position of the hit
    T z; // coordinate of station
};

template <typename T> struct LFTrackParam
{
    LFTrackParam() : z(0)
    {
        p[0] = 0;
        p[1] = 0;
    }

    LFTrackParam(T x_, T tx_, T z_) : z(z_)
    {
        p[0] = x_;
        p[1] = tx_;
    }

    T X(T z_) const
    {
        return p[1] * (z_ - z) + p[0];
    }

    const T& X() const
    {
        return p[0];
    }

    const T& Tx() const
    {
        return p[1];
    }

    const T& Z() const
    {
        return z;
    }

    T& X()
    {
        return p[0];
    }

    T& Tx()
    {
        return p[1];
    }

    T& Z()
    {
        return z;
    }

    T p[2], z; // p[0] = x, p[1] = tx, z

    void Print()
    {
        std::cout << z << " " << p[0] << " " << p[1] << std::endl;
    }
};

template <typename T> struct LFTrackCovMatrix
{
    T& C00()
    {
        return c[0];
    }

    T& C10()
    {
        return c[1];
    }

    T& C11()
    {
        return c[2];
    }

    T c[3]; // C00, C10, C11

    void Print()
    {
        std::cout << c[0] << " " << c[1] << " " << c[2] << std::endl;
    }
};

template <typename T1, typename T2> struct LFTrack
{
    std::vector<LFPoint<T1> > hits;

    LFTrackParam<T1> rParam; // track parameters reconstructed by the fitter
    LFTrackCovMatrix<T1> rCovMatrix; // error (or covariance) matrix

    T1 chi2; // chi-squared deviation between points and trajectory
    T2 ndf; // number degrees of freedom

    std::vector<LFTrackParam<T1> > mcPoints; // simulated track parameters
};

template <typename T> struct LFGeometry
{
    int GetNStations() const
    {
        return zs.size();
    }

    std::vector<T> zs; // z of a station
};

// Return simulated track with given parameters

template <typename T1, typename T2> class LFSimulator
{
public:
    LFSimulator() : fGeometry(), fSigma(DSigma)
    {
        fGeometry.zs.resize(DNStations);

        for (int i = 0; i < DNStations; ++i)
          {
              fGeometry.zs[i] = (DDZ * static_cast<T1>(i));
          }
    }

    ~LFSimulator() {}

    inline void Simulate(const LFTrackParam<T1>& param, LFTrack<T1, T2>& track);
    inline void Simulate(const LFTrackParam<T1>& param, LFTrack<T1, T2>* track, int N);

    void SetGeometry(const LFGeometry<T1>& g)
    {
        fGeometry = g;
    }

    void SetSigma(const T1& s)
    {
        fSigma = s;
    }

private:
    LFGeometry<T1> fGeometry;
    T1 fSigma;
};

template <typename T1, typename T2> inline void LFSimulator<T1, T2>::Simulate(const LFTrackParam<T1>& param, LFTrack<T1, T2>& track)
{
    track.mcPoints.clear();
    LFTrackParam<T1> curMCPoint = param;
    for (int i = 0; i < fGeometry.GetNStations(); ++i)
    {
        const T1 z = fGeometry.zs[i];
        // propagate to next station
        curMCPoint.X() = curMCPoint.X(z);
        curMCPoint.Z() = z;
        // save
        track.mcPoints.push_back(curMCPoint);
    }

    track.hits.clear();

    for (int i = 0; i < fGeometry.GetNStations(); ++i)
    {
        const T1 z = track.mcPoints[i].z;
        T1 x = NAN0;
        // distribute hits uniformly
        {
            const T1 width = 2 * fSigma * sqrt(3); // sqrt(12)
            x = track.mcPoints[i].X() + (T1(rand()) / RAND_MAX - 0.5) * width;
        }

        track.hits.push_back(LFPoint<T1>(x, z));
    }
}

template <typename T1, typename T2> inline void LFSimulator<T1, T2>::Simulate(const LFTrackParam<T1>& param, LFTrack<T1, T2>* track, int N)
{
    for (int i = 0; i < N; ++i)
    {
        Simulate(param, track[i]);
    }
}

template <typename T1, typename T2> class LFFitter
{
public:
    LFFitter() : fSigma(DSigma)
    {
    }

    inline void Fit(LFTrack<T1, T2>& track) const;

    void SetSigma(const T1& s)
    {
        fSigma = s;
    }

private:

    inline void Initialize(LFTrack<T1, T2>& track) const;
    inline void Extrapolate(LFTrack<T1, T2>& track, T1 z) const;
    inline void Filter(LFTrack<T1, T2>& track, T1 x) const;

    T1 fSigma;
};


template <typename T1, typename T2> void LFFitter<T1, T2>::Fit(LFTrack<T1, T2>& track) const
{
    Initialize(track);

    const int NHits = track.hits.size();

    for (int i = 0; i < NHits; ++i)
    {
        Extrapolate(track, track.hits[i].z);
        Filter(track, track.hits[i].x);
    }

    Extrapolate(track, track.mcPoints.back().z); // exptrapolate to MC point for comparison with MC info
}

template <typename T1, typename T2> void LFFitter<T1, T2>::Initialize(LFTrack<T1, T2>& track) const
{
    track.rParam.Z() = 0;
    track.rParam.X() = 0;
    track.rParam.Tx() = 0;
    track.chi2 = 0;
    track.ndf = -2;

    track.rCovMatrix.C00() = InfX;
    track.rCovMatrix.C10() = 0;
    track.rCovMatrix.C11() = InfTx;
}

template <typename T1, typename T2> void LFFitter<T1, T2>::Extrapolate(LFTrack<T1, T2>& track, T1 z_) const
{
    T1 &z = track.rParam.Z();
    T1 &x = track.rParam.X();
    T1 &tx = track.rParam.Tx();
    T1 &C00 = track.rCovMatrix.C00();
    T1 &C10 = track.rCovMatrix.C10();
    T1 &C11 = track.rCovMatrix.C11();

    const T1 dz = z_ - z;

    x += dz * tx;
    z = z_;

    const T1 C10_in = C10;
    C10 += dz * C11;
    C00 += dz * (C10 + C10_in);
}

template <typename T1, typename T2> void LFFitter<T1, T2>::Filter(LFTrack<T1, T2>& track, T1 x_) const
{
    T1 &x = track.rParam.X();
    T1 &tx = track.rParam.Tx();
    T1 &C00 = track.rCovMatrix.C00();
    T1 &C10 = track.rCovMatrix.C10();
    T1 &C11 = track.rCovMatrix.C11();
    // H = { 1, 0 }
    // zeta = Hr - r // P.S. not "r - Hr" here becase later will be rather "r = r - K * zeta" then  "r = r + K * zeta"
    T1 zeta = x - x_;
    // F = C*H'
    T1 F0 = C00;
    T1 F1 = C10;
    // H*C*H'
    T1 HCH = F0;
    // S = 1. * ( V + H*C*H' )^-1
    T1 wi = 1. / (fSigma * fSigma + HCH);
    T1 zetawi = zeta * wi;
    track.chi2 += zeta * zetawi;
    track.ndf += 1;
    // K = C*H'*S = F*S
    T1 K0 = F0 * wi;
    T1 K1 = F1 * wi;
    // r = r - K * zeta
    x -= K0 * zeta;
    tx -= K1 * zeta;
    // C = C - K*H*C = C - K*F
    C00 -= K0 * F0;
    C10 -= K1 * F0;
    C11 -= K1 * F1;
}

#ifdef SIMDIZED

void CopyToSIMDVector(LFTrack<float, int>* from, LFTrack<fvec, fvec>* to, const int n)
{
    for (int i = 0; i < n; ++i)
    {
        unsigned int hitsSize = from[i].hits.size();

        LFTrack<fvec, fvec>& target = to[i];

        target.hits.resize(hitsSize); target.mcPoints.resize(hitsSize);

        for (int v = 0; v < fvecLen; ++v)
        {
            const LFTrack<float, int>& source = from[(i << 2) + v];

            for (int h = 0; h < hitsSize; ++h)
            {
                target.hits[h].x[v] = source.hits[h].x;
                target.hits[h].z = source.hits[h].z;
            }

            target.mcPoints[hitsSize - 1].z = source.mcPoints[hitsSize - 1].z;
        }
    }
}

void CopyToScalarVector(LFTrack<fvec, fvec>* from, LFTrack<float, int>* to, const int n)
{
    for (int i = 0; i < n; ++i)
    {
        const LFTrack<fvec, fvec>& source = from[i];

        for (int v = 0; v < fvecLen; ++v)
        {
            LFTrack<float, int>& target = to[(i << 2) + v];

            target.chi2 = source.chi2[v];
            target.ndf = source.ndf[v];

            target.rParam.z = source.rParam.z[v];

            target.rParam.p[0] = source.rParam.p[0][v];
            target.rParam.p[1] = source.rParam.p[1][v];

            for (size_t a = 0; a < 3; a++)
            {
                target.rCovMatrix.c[a] = source.rCovMatrix.c[a][v];
            }
        }
    }
}

#endif

int main(int arc, char** argv)
{
    std::cout << "== [*HPC-PR*] KFLineFitter (Kalman Filter)" << std::endl;
    std::cout << "-- ---------- ---------------------------------" << std::endl;

    // Number of stations
    const int NStations = 10;
    // Distance between stations in standard geometry
    const float DZ = 10;
    // Sigma of hit distribution
    const float Sigma = 0.5;

    std::cout << "== [*HPC-PR*] Setting NStations to " << NStations << std::endl;
    std::cout << "== [*HPC-PR*] Setting DZ to " << DZ << std::endl;
    std::cout << "== [*HPC-PR*] Setting Sigma to " << Sigma << std::endl;

    std::cout << "-- ---------- ---------------------------------" << std::endl;

    LFGeometry<float> geo; geo.zs.resize(NStations);

    for (int i = 0; i < NStations; ++i)
    {
        geo.zs[i] = (DZ * static_cast<float>(i));
    }

    LFSimulator<float, int> sim;

    sim.SetGeometry(geo); sim.SetSigma(Sigma);

    /* SIMULATE TRACKS */

    const int NTracks = 10000; LFTrack<float, int> tracks[NTracks];

    for (int i = 0; i < NTracks; ++i)
    {
        LFTrack<float, int> &track = tracks[i];

        LFTrackParam<float> par;
        par.Z() = 0;
        par.X() = (float(rand()) / RAND_MAX - 0.5) * 5;
        par.Tx() = (float(rand()) / RAND_MAX - 0.5) * 0.2;

        sim.Simulate(par, track);
    }

#ifndef SIMDIZED

    LFFitter<float, int> fit;

#else

    LFFitter<fvec, fvec> fit;
    const int simdsize
    {
        NTracks >> 2
    };
    LFTrack<fvec, fvec> SIMDTracks[simdsize];
    CopyToSIMDVector
    (
        tracks, SIMDTracks, simdsize
    );

#endif

    TStopwatch timer;
    fit.SetSigma(Sigma);

#ifdef TIME

    timer.Start(1);

#endif

#ifndef SIMDIZED

    for (int i = 0; i < NTracks; ++i)
    {
        LFTrack<float, int> &track = tracks[i]; fit.Fit(track);
    }

#else

    for (int i = 0; i < simdsize; ++i)
    {
        LFTrack<fvec, fvec> &track = SIMDTracks[i]; fit.Fit(track);
    }

#endif

#ifdef TIME

    timer.Stop();

#endif

#ifdef SIMDIZED

    CopyToScalarVector
    (
       SIMDTracks, tracks, simdsize
    );

#endif

#ifdef OUTPUT

    std::ofstream fout; fout.open("output.txt", std::fstream::out);

    for (int i = 0; i < NTracks; ++i)
    {
        LFTrack<float, int> &track = tracks[i];
        fout << i << std::endl; // << "Track: "
        fout << track.mcPoints.back().Z() << " " << track.mcPoints.back().X() << " " << track.mcPoints.back().Tx() << std::endl; // << "MC: "
        fout << track.rParam.Z() << " " << track.rParam.X() << " " << track.rParam.Tx() << std::endl; // << "Reco: "
        fout << track.rCovMatrix.C00() << " " << track.rCovMatrix.C10() << " " << track.rCovMatrix.C11() << std::endl;
    }

    fout.close();

#endif

#ifdef TIME

    std::cout << "== [*HPC-PR*] KFLineFitter took " << timer.RealTime() * 1000 << " ms" << std::endl;

#endif

    return 0;
}
